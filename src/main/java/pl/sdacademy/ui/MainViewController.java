package pl.sdacademy.ui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import pl.sdacademy.dao.ProductsDao;
import pl.sdacademy.shoppings.Products;

/**
 * Created by Konrad on 16.05.2017.
 */
public class MainViewController {

    @FXML
    private ListView list1;
    @FXML
    private Label label1;
    @FXML
    private Button button1;
    @FXML
    private Button button2;


    public void initialize() {
        label1.setText("Shopping List");
        button1.setText("Just bought");
        button2.setText("Load another shopping list");
        button2.setOnAction(event -> {



        });

    }
}
