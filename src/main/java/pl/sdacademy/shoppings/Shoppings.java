package pl.sdacademy.shoppings;

/**
 * Created by Konrad on 17.05.2017.
 */
public class Shoppings {
    public Integer id;

    public Shoppings(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
