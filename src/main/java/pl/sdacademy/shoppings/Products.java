package pl.sdacademy.shoppings;

import javax.swing.text.html.ListView;

/**
 * Created by Konrad on 17.05.2017.
 */
public class Products extends Shoppings{

    public String productName;
    public int quantity;
    public ListView products;

    public ListView getProducts() {
        return products;
    }

    public void setProducts(ListView products) {
        this.products = products;
    }

    public Products(Integer id, String productName, int quantity) {
        super(id);
        this.productName = productName;
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
