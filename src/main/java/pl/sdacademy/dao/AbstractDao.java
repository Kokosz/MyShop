package pl.sdacademy.dao;

import pl.sdacademy.shoppings.Shoppings;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Konrad on 17.05.2017.
 */
public abstract class AbstractDao <T extends Shoppings>{
    protected List<T> products;
    public AbstractDao() {
        products = new ArrayList<>();

        try {
            Path filePath = Paths.get(getFileName());
            if(!Files.exists(filePath)) {
                Files.createFile(filePath);
            }
            Stream<String> linesStream = Files.lines(filePath);
            linesStream.forEach(l -> products.add(createProductsFromFileLine(l)));

//			List<String> fileLines = Files.readAllLines(Paths.get(getFileName()));
//			for (String fileLine : fileLines) {
//				entities.add(createEntityFromFileLine(fileLine));
//			}

            linesStream.close();
        } catch (IOException e) {
            throw new RuntimeException("Bład odczytu encji"
                    + " z pliku " + getFileName());
        }
    }
    protected abstract String getFileName();
    protected abstract T createProductsFromFileLine(String fileLine);
    protected abstract String createFileLineFromProduct(T products);

}
