package pl.sdacademy.dao;

import pl.sdacademy.shoppings.Products;
import pl.sdacademy.shoppings.Shoppings;

import java.time.LocalDate;

/**
 * Created by Konrad on 17.05.2017.
 */
public class ProductsDao extends AbstractDao<Products>{
    @Override
    protected String getFileName() {
        return "shoppingList.txt";
    }

    @Override
    protected Products createProductsFromFileLine(String fileLine) {
        String[] lineValues = fileLine.split(",");
        int id = Integer.parseInt(lineValues[0]);
        String productName = lineValues[1];
        int quantity = Integer.parseInt(lineValues[2]);
        return new Products(id, productName, quantity);
    }

    @Override
    protected String createFileLineFromProduct(Products products) {
        return products.getId() + "," + products.getProductName() + "," + products.getQuantity();
    }

}
